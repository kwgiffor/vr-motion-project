﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    
    public Transform headTrans;
    public Transform camera;
    public Transform cube;

    public float distance;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        distance = Vector3.Distance(headTrans.position, cube.position);

        GetComponent<Camera>().fieldOfView = 10*distance + 60;

        camera.rotation = Quaternion.Inverse(cube.rotation) * headTrans.rotation;


    }

}
